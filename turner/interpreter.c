#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "kvec.h"

/**** UTILS ****/

__attribute__((noreturn))
static void panic(const char *msg)
{
	printf("Panic: %s\n", msg);
	abort();
}

static void* mymalloc(size_t size)
{
	void* data = malloc(size);
	if (data == NULL) panic("Not enough memory: malloc returned NULL.");
	return data;
}

typedef struct ast_node_t ast_node_t;
typedef struct token_t token_t;
typedef struct stringtable_entry_t stringtable_entry_t;

typedef struct environment_t {
	const char *filename;
	unsigned line;
	unsigned column;
	const char *lexer_start;
	const char *lexer_pos;
	const char *module_name;
	stringtable_entry_t *stringtable_head;
	ast_node_t *ast;
	token_t *head_token;
} environment_t;

/**** LEXER ****/

typedef enum tok_type {
	tok_invalid,
	tok_IDENT,
	tok_STRINGLIT,
	tok_NUMBERLIT,
	tok_EOF,
	tok_CURLY_OPEN,
	tok_CURLY_CLOSE,
	tok_PARENS_OPEN,
	tok_PARENS_CLOSE,
	tok_BRACKETS_OPEN,
	tok_BRACKETS_CLOSE,
	tok_PLUS,
	tok_MINUS,
	tok_ASTERISK,
	tok_SLASH,
	tok_PERCENT,
	tok_CIRCUMFLEX,
	tok_TILDE,
	tok_SEMICOLON,
	tok_COLON,
	tok_COLONEQ,
	tok_COMMA,
	tok_EXCLAM,
	tok_EXCLAMEQ,
	tok_DOT,
	tok_DOTDOT,
	tok_AMP,
	tok_AMPAMP,
	tok_BAR,
	tok_BARBAR,
	tok_EQ,
	tok_EQEQ,
	tok_LTEQ,
	tok_GTEQ,
	tok_LT,
	tok_GT,
	/* keywords */
	tok_IMPORT,
	tok_FUN,
	tok_GEN,
	tok_VAL,
	tok_VAR,
	tok_IF,
	tok_RETURN,
	tok_LOOP,
	tok_FOR,
	tok_IN,
	tok_CONTINUE,
	tok_BREAK,
	tok_THEN,
	tok_ELSE,
	tok_TRUE,
	tok_FALSE,
	tok_AND,
	tok_OR,
} tok_type;

struct stringtable_entry_t {
	struct stringtable_entry_t *next;
	tok_type type;
	const char *string;
};

static stringtable_entry_t* initial_entry(tok_type type, const char* string, stringtable_entry_t *pred) {
	stringtable_entry_t *const entry = mymalloc(sizeof(stringtable_entry_t));
	entry->type = type;
	entry->string = string;
	entry->next = pred;
	return entry;
}

static void stringtable_init(environment_t *env)
{
	stringtable_entry_t *c = NULL;
	c = initial_entry(tok_IMPORT,    "import",   c);
	c = initial_entry(tok_FUN,       "fun",      c);
	c = initial_entry(tok_GEN,       "gen",      c);
	c = initial_entry(tok_VAL,       "val",      c);
	c = initial_entry(tok_VAR,       "var",      c);
	c = initial_entry(tok_IF,        "if",       c);
	c = initial_entry(tok_RETURN,    "return",   c);
	c = initial_entry(tok_LOOP,      "loop",     c);
	c = initial_entry(tok_BREAK,     "break",    c);
	c = initial_entry(tok_CONTINUE,  "continue", c);
	c = initial_entry(tok_FOR,       "for",      c);
	c = initial_entry(tok_IN,        "in",       c);
	c = initial_entry(tok_THEN,      "then",     c);
	c = initial_entry(tok_ELSE,      "else",     c);
	c = initial_entry(tok_TRUE,      "true",     c);
	c = initial_entry(tok_FALSE,     "false",    c);
	c = initial_entry(tok_AND,       "and",      c);
	c = initial_entry(tok_OR,        "or",      c);
	env->stringtable_head = c;
}

static stringtable_entry_t* stringtable_insert(const char *string, environment_t *env)
{
	stringtable_entry_t *current = env->stringtable_head;
	// TODO linear search is inefficient for a stringtable implementation
	for (;;) {
		if (strcmp(current->string, string) == 0) { /* found! */
			return current;
		}
		if (current->next == NULL) { /* string not in table: insert! */
			stringtable_entry_t *entry = mymalloc(sizeof(stringtable_entry_t));
			entry->type = tok_IDENT;
			entry->string = string;
			entry->next = NULL;
			return entry;
		}
		current = current->next;
	}
}

static void environment_init(environment_t *env)
{
	env->line = 0;
	env->column = 0;
	stringtable_init(env);
}

static void skip_whitespace_and_comments(const char **e_pos, unsigned *e_line, unsigned *e_col) {
	const char *pos = *e_pos;
	unsigned line   = *e_line;
	unsigned col    = *e_col;
	for (;;) {
		switch (*pos) {
			case ' ':
			case '\t':
			case '\r':
				pos++; col++;
				continue;
			case '\n':
				pos++;
				line++; col = 0;
				continue;
			case '/':
				pos++; col++;
				if (*pos == '/') { /* skip slash-slash comment */
					for (;;) {
						pos++; col++;
						if (*pos == '\n') {
							pos++;
							line++; col = 0;
							break;
						}
						if (*pos == EOF) break;
					}
					continue;
				} else if (*pos == '*') { /* skip slash-asterisk comment */
					for (;;) {
						pos++; col++;
						if (*pos == '\n') {
							line++; col = 0;
						} else if (*pos == EOF) {
							break;
						} else if (*pos == '*' && *(pos+1) == '/') {
							pos += 2; col += 2;
							break;
						}
					}
					continue;
				}
				pos--; col--;
				assert (*pos == '/');
				/* fallthrough: slash is no whitespace */
			default:
				*e_pos  = pos;
				*e_line = line;
				*e_col  = col;
				return;
		}
	}
}

struct token_t {
	tok_type type;
	struct token_t *next;
	const char *string;
	unsigned line;
	unsigned column;
};

static token_t* tok_create(tok_type type, unsigned line, unsigned column)
{
	token_t *token = mymalloc(sizeof(token_t));
	token->type = type;
	token->string = NULL;
	token->next = NULL;
	token->line = line;
	token->column = column;
	return token;
}

static unsigned lex(environment_t *env)
{
	token_t *prev = mymalloc(sizeof(token_t));
	env->head_token = prev;
	const char *pos = env->lexer_pos;
	unsigned line = 0;
	unsigned column = 0;
	for (;;) {
		skip_whitespace_and_comments(&pos, &line, &column);
		token_t *token;
		switch (*pos) {
			case '\0':
			case EOF:
				token = tok_create(tok_EOF, line, column);
				prev->next = token;
				/* first token was a dummy, remove it */
				token_t *dummy = env->head_token;
				env->head_token = dummy->next;
				free(dummy);
				return 0;
			case '{': token = tok_create(tok_CURLY_OPEN,     line, column); break;
			case '}': token = tok_create(tok_CURLY_CLOSE,    line, column); break;
			case '(': token = tok_create(tok_PARENS_OPEN,    line, column); break;
			case ')': token = tok_create(tok_PARENS_CLOSE,   line, column); break;
			case '[': token = tok_create(tok_BRACKETS_OPEN,  line, column); break;
			case ']': token = tok_create(tok_BRACKETS_CLOSE, line, column); break;
			case '+': token = tok_create(tok_PLUS,           line, column); break;
			case '-': token = tok_create(tok_MINUS,          line, column); break;
			case '*': token = tok_create(tok_ASTERISK,       line, column); break;
			case '/': token = tok_create(tok_SLASH,          line, column); break;
			case '%': token = tok_create(tok_PERCENT,        line, column); break;
			case '^': token = tok_create(tok_CIRCUMFLEX,     line, column); break;
			case '~': token = tok_create(tok_TILDE,          line, column); break;
			case ';': token = tok_create(tok_SEMICOLON,      line, column); break;
			case ',': token = tok_create(tok_COMMA,          line, column); break;
			case ':':
						if (*(pos+1) == '=') {
							token = tok_create(tok_COLONEQ,  line, column);
							pos++; column++; break;
						} else {
							token = tok_create(tok_COLON,    line, column);
							break;
						}
			case '.':
						if (*(pos+1) == '.') {
							token = tok_create(tok_DOTDOT,   line, column);
							pos++; column++; break;
						} else {
							token = tok_create(tok_DOT,      line, column);
							break;
						}
			case '&':
						if (*(pos+1) == '&') {
							token = tok_create(tok_AMPAMP,   line, column);
							pos++; column++; break;
						} else {
							token = tok_create(tok_AMP,      line, column);
							break;
						}
			case '|':
						if (*(pos+1) == '|') {
							token = tok_create(tok_BARBAR,   line, column);
							pos++; column++; break;
						} else {
							token = tok_create(tok_BAR,      line, column);
							break;
						}
			case '<':
						if (*(pos+1) == '=') {
							token = tok_create(tok_LTEQ,     line, column);
							pos++; column++; break;
						} else {
							token = tok_create(tok_LT,       line, column);
							break;
						}
			case '>':
						if (*(pos+1) == '=') {
							token = tok_create(tok_GTEQ,     line, column);
							pos++; column++; break;
						} else {
							token = tok_create(tok_GT,       line, column);
							break;
						}
			case '!':
						if (*(pos+1) == '=') {
							token = tok_create(tok_EXCLAMEQ, line, column);
							pos++; column++; break;
						} else {
							token = tok_create(tok_EXCLAM,   line, column);
							break;
						}
			case '=':
						if (*(pos+1) == '=') {
							token = tok_create(tok_EQEQ,     line, column);
							pos++; column++; break;
						} else {
							token = tok_create(tok_EQ,       line, column);
							break;
						}
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
			case 'g': case 'h': case 'i': case 'j': case 'k': case 'l':
			case 'm': case 'n': case 'o': case 'p': case 'q': case 'r':
			case 's': case 't': case 'u': case 'v': case 'w': case 'x':
			case 'z':
			case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
			case 'G': case 'H': case 'I': case 'J': case 'K': case 'L':
			case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R':
			case 'S': case 'T': case 'U': case 'V': case 'W': case 'X':
			case 'Z': {
							 const char *start = pos;
							 const unsigned col = column;
							 for (;;) {
								 const char cur = *pos;
								 pos++; column++;
								 if (cur >= 'a' && cur<='z') continue;
								 if (cur >= 'A' && cur<='Z') continue;
								 pos--; column--;
								 break;
							 }
							 const unsigned length = pos-start+1;
							 char *string = mymalloc(length);
							 memcpy(string, start, length-1);
							 string[length-1] = '\0';
							 stringtable_entry_t *ret = stringtable_insert(string, env);
							 token = tok_create(ret->type, line, col);
							 token->string = ret->string;
							 pos--; column--;
							 break;
						 }
			case '"': {
							 pos++; column++;
							 const char *start = pos;
							 const unsigned col = column;
							 for (;;) {
								 char cur = *pos;
								 if (cur == '"' || cur == EOF) break;
								 pos++; column++;
								 if (cur != '\\') continue;
								 pos++; column++;
							 }
							 const unsigned length = pos-start;
							 char *string = mymalloc(length);
							 memcpy(string, start, length-1);
							 string[length-1] = '\0';
							 stringtable_entry_t *ret = stringtable_insert(string, env);
							 token = tok_create(tok_STRINGLIT, line, col);
							 token->string = ret->string;
							 break;
						 }
			case '0':
			case '1': case '2': case '3': case '4': case '5':
			case '6': case '7': case '8': case '9':
						 {
							 const char *start = pos;
							 const unsigned col = column;
							 for (;;) {
								 const char cur = *pos++; column++;
								 if (cur >= '0' && cur <= '9') continue;
								 break;
							 }
							 if (*pos == '.') {
								 const char dot = *(pos+1);
								 if (dot >= '0' && dot <= '9')
									 for (;;) {
										 const char cur = *pos++; column++;
										 if (cur >= '0' && cur <= '9') continue;
										 break;
									 }
							 }
							 const unsigned length = pos-start;
							 char *string = mymalloc(length);
							 memcpy(string, start, length-1);
							 string[length-1] = '\0';
							 token = tok_create(tok_NUMBERLIT, line, col);
							 token->string = string;
							 pos-=2; column-=2;
							 break;
						 }
			default:
						 printf("Unknown char to lex '%d' at %d:%d\n", *(pos-1), line, column);
						 token->type = tok_invalid;
						 return 1;
		}
		pos++; column++;
		prev->next = token;
		prev = token;
	}
}

/**** PARSER ****/

/**
Program := Statement*
Statement := Block
           | ValDecl
           | VarDecl
           | ProcedureCall
           | Assignment
           | Loop
           | ForLoop
           | IfStatement
           | ReturnStatement
           | BreakStatement
           | ContinueStatement
Block := '{' Statement* '}'
ValDecl := 'val' IDENT '=' Expression
VarDecl := 'var' IDENT '=' Expression
ProcedureCall := IDENT '(' (Expression (',' Expression)*)? ')'
Assignment := IDENT ':=' Expression
Loop := 'loop' Expression? Block ('then' Block)? ('else' Block)?
ForLoop := 'for' IDENT 'in' Expression Block ('then' Block)? ('else' Block)?
IfStatement := 'if' Expression Block ('else' Block)?
ReturnStatement := 'return' Expression?
BreakStatement := 'break' NUMBER?
ContinueStatement := 'continue' NUMBER?
Expression := IDENT
            | true
            | false
            | STRING
            | NUMBER
            | '(' Expression ')'
            | Generator
            | Function
            | FunctionCall
            | UnaryOp
            | BinaryOp
Generator := 'gen' Parameters Block
Function  := 'fun' Parameters ('=' Expression | Block)
Parameters := '(' (Parameter (',' Parameter)*)? ')'
Parameter  := IDENT ':' Expression
UnaryOp := UNOP Expression
BinaryOp := Expression BINOP Expression
FunctionCall := IDENT '(' (Expression (',' Expression)*)? ')'

**/

typedef kvec_t(ast_node_t*) ast_node_vec_t;

typedef enum ast_node_type {
	ast_NONE,
	ast_ERROR,
	ast_NOP,
	ast_DECL,
	ast_BLOCK,
	ast_IDENT,
	ast_UNOP,
	ast_BINOP,
	ast_NUMBER,
	ast_STRING,
	ast_CALL,
	ast_IF,
	ast_FOR,
	ast_LOOP,
	ast_BREAK,
	ast_CONTINUE,
	ast_RETURN,
	ast_TYPED,
	ast_FUN,
	ast_GEN,
	ast_ASSIGNMENT,
	ast_ARRAYACCESS,
	ast_MULTI, /* multiple statements */
} ast_node_type;

struct ast_node_t {
	ast_node_type type;
	union extended_ast_attributes {
		struct {
			bool single_assign;
			const char *string;
			const char *type;
			const struct ast_node_t *expr;
			ast_node_vec_t list;
		} named;
		struct {
			tok_type type;
			const struct ast_node_t *lhs;
			const struct ast_node_t *rhs;
		} op;
		struct {
			const char *string;
			const struct ast_node_t *op1;
			const struct ast_node_t *op2;
			const struct ast_node_t *op3;
			const struct ast_node_t *op4;
		} operands;
	} t;
};

static ast_node_t *ast_node_create(ast_node_type t)
{
	ast_node_t *node = mymalloc(sizeof(ast_node_t));
	node->type = t;
	return node;
}

static token_t* peek(const environment_t *env, const unsigned n)
{
	token_t *t = env->head_token;
	for (int i=n; i>0; i--) {
		if (t->next == NULL) return NULL;
		t = t->next;
	}
	return t;
}

static token_t* token(const environment_t *env)
{
	return peek(env, 0);
}

static unsigned next_token(environment_t *env)
{
	token_t *t = env->head_token;
	if (t == NULL) return 1;
	env->head_token = t->next;
	return 0;
}

static ast_node_t* ast_error(const char *msg, environment_t *env)
{
	next_token(env);
	ast_node_t *node = ast_node_create(ast_ERROR);
	node->t.named.string = msg;
	return node;
}

static bool check_and_skip(tok_type type, environment_t *env)
{
	token_t *t = token(env);
	if (NULL == t) return false;
	if (t->type != type) return false;
	next_token(env);
	return true;
}

static void must_skip(tok_type type, environment_t *env)
{
	if (check_and_skip(type, env)) return;
	panic("skip inconsistency");
}

static ast_node_t* parse_block(environment_t *env);
static ast_node_t* parse_function_call(environment_t *env);
static ast_node_t* parse_statement(environment_t *env);
static ast_node_t* parse_expression(environment_t *env);

static token_t* parse_ident(environment_t *env)
{
	token_t *ident = token(env);
	if (ident->type != tok_IDENT) return NULL;
	next_token(env);
	return ident;
}

static ast_node_t* parse_array_access(environment_t *env)
{
	token_t *ident = parse_ident(env);
	if (NULL == ident)
		return ast_error("expected ident", env);
	if (!check_and_skip(tok_BRACKETS_OPEN, env))
		return ast_error("expected '['", env);
	ast_node_t *expr = parse_expression(env);
	if (!check_and_skip(tok_BRACKETS_CLOSE, env))
		return ast_error("expected ']'", env);
	ast_node_t *arraccess = ast_node_create(ast_ARRAYACCESS);
	arraccess->t.named.string = ident->string;
	arraccess->t.named.expr   = expr;
	return arraccess;
}

static unsigned get_precedence(tok_type t)
{
	switch (t) {
		case tok_DOTDOT: return 2;
		case tok_ASTERISK: return 3;
		case tok_SLASH: return 3;
		case tok_PERCENT: return 3;
		case tok_PLUS: return 4;
		case tok_MINUS: return 4;
		case tok_LT: return 6;
		case tok_LTEQ: return 6;
		case tok_GT: return 6;
		case tok_GTEQ: return 6;
		case tok_EQEQ: return 7;
		case tok_EXCLAMEQ: return 7;
		case tok_AMP: return 8;
		case tok_CIRCUMFLEX: return 9;
		case tok_BAR: return 10;
		case tok_AMPAMP: return 11;
		case tok_BARBAR: return 11;
		case tok_AND: return 11;
		case tok_OR: return 11;
		default: return 0;
	}
}

static ast_node_t* parse_typed_ident(environment_t *env)
{
	token_t *ident = parse_ident(env);
	if (NULL == ident)
		return ast_error("expected ident", env);
	if (!check_and_skip(tok_COLON, env))
		return ast_error("expected ':'", env);
	token_t *type = parse_ident(env);
	if (NULL == type)
		return ast_error("expected ident", env);
	ast_node_t *typed = ast_node_create(ast_TYPED);
	typed->t.named.string = ident->string;
	typed->t.named.type   = type->string;
	return typed;
}

static ast_node_t *parse_FunGen(tok_type t, environment_t *env)
{
	must_skip(t, env);
	if (!check_and_skip(tok_PARENS_OPEN, env))
		return ast_error("expected '('", env);
	ast_node_vec_t args;
	kv_init(args);
	if (!check_and_skip(tok_PARENS_CLOSE, env)) {
		for (;;) {
			kv_push(ast_node_t*, args, parse_typed_ident(env));
			if (check_and_skip(tok_COMMA, env)) continue;
			if (check_and_skip(tok_PARENS_CLOSE, env)) break;
			return ast_error("unexpected token", env);
		}
	}
	if (!check_and_skip(tok_COLON, env))
		return ast_error("expected ':'", env);
	token_t *ident = parse_ident(env);
	if (NULL == ident)
		return ast_error("expected ident", env);
	ast_node_t *body = parse_block(env);
	ast_node_t *fungen = ast_node_create(ast_FUN);
	fungen->t.named.list = args;
	fungen->t.named.expr = body;
	return fungen;
}

static ast_node_t* parse_fun(environment_t *env)
{
	return parse_FunGen(tok_FUN, env);
}

static ast_node_t* parse_gen(environment_t *env)
{
	return parse_FunGen(tok_GEN, env);
}

static ast_node_t* parse_json(environment_t *env)
{
	must_skip(tok_CURLY_OPEN, env);
	panic("not implemented yet");
}

static ast_node_t* parse_primary_expression(environment_t *env)
{
	const token_t *current = token(env);
	switch (current->type) {
		case tok_IDENT:
			{
				const token_t *next = peek(env, 1);
				switch (next->type) {
					case tok_PARENS_OPEN:
						return parse_function_call(env);
					case tok_BRACKETS_OPEN:
						return parse_array_access(env);
					default:
						next_token(env);
						ast_node_t *ident = ast_node_create(ast_IDENT);
						ident->t.named.string = current->string;
						return ident;
				}
			}
		case tok_NUMBERLIT:
			{
				next_token(env);
				ast_node_t *lit = ast_node_create(ast_NUMBER);
				lit->t.named.string = current->string;
				return lit;
			}
		case tok_STRINGLIT:
			{
				next_token(env);
				ast_node_t *lit = ast_node_create(ast_NUMBER);
				lit->t.named.string = current->string;
				return lit;
			}
		case tok_EXCLAM:
		case tok_TILDE:
		case tok_MINUS:
			{
				ast_node_t *lit = ast_node_create(ast_UNOP);
				next_token(env);
				ast_node_t *expr = parse_primary_expression(env);
				lit->t.op.lhs = expr;
				lit->t.op.rhs = NULL; /* only for binary op */
				lit->t.op.type = current->type;
				return lit;
			}
		case tok_FUN: // anonymous function
			{
				return parse_fun(env);
			}
		case tok_GEN: // anonymous generator
			{
				return parse_gen(env);
			}
		case tok_CURLY_OPEN: // literal JSON hashmap
			{
				return parse_json(env);
			}
		case tok_PARENS_OPEN:
			{
				must_skip(tok_PARENS_OPEN, env);
				ast_node_t *expr = parse_expression(env);
				if (!check_and_skip(tok_PARENS_CLOSE, env))
					return ast_error("expected ')'", env);
				return expr;
			}
		default: /* expected something different for expression */
			return ast_error("invalid expression start token", env);
	}
	return ast_error("reached unreachable code", env);
}

static ast_node_t* parse_binop_expression(const ast_node_t *lhs, environment_t *env)
{
	const token_t *op = token(env);
	const unsigned precedence = get_precedence(op->type);
	if (0 == precedence)
		return ast_error("unknown precedence", env);
	next_token(env);
	ast_node_t *rhs = parse_primary_expression(env);
	const token_t *nextop = token(env);
	unsigned nextprecedence = get_precedence(nextop->type);
	while (precedence <= nextprecedence) {
		if (0 == nextprecedence)
			return ast_error("unknown precedence", env);
		rhs = parse_binop_expression(rhs, env);
		nextop = token(env);
		nextprecedence = get_precedence(nextop->type);
	}
	ast_node_t *binop = ast_node_create(ast_BINOP);
	binop->t.op.lhs = lhs;
	binop->t.op.rhs = rhs;
	binop->t.op.type = op->type;
	return binop;
}

static ast_node_t* parse_expression(environment_t *env)
{
	ast_node_t *lhs = parse_primary_expression(env);
	token_t *tok = token(env);
	if (NULL == tok) return lhs;
	if (get_precedence(tok->type) != 0) { /* no precedence, no binop */
		return parse_binop_expression(lhs, env);
	} else {
		return lhs;
	}
}

static ast_node_t* parse_function_call(environment_t *env)
{
	token_t *ident = parse_ident(env);
	if (NULL == ident)
		return ast_error("expected ident", env);
	if (!check_and_skip(tok_PARENS_OPEN, env))
		return ast_error("expected '('", env);
	ast_node_vec_t args;
	kv_init(args);
	if (!check_and_skip(tok_PARENS_CLOSE, env)) {
		for (;;) {
			kv_push(ast_node_t*, args, parse_expression(env));
			if (check_and_skip(tok_COMMA, env)) continue;
			if (check_and_skip(tok_PARENS_CLOSE, env)) break;
			ast_error("unexpected token", env);
		}
	}
	ast_node_t *call = ast_node_create(ast_CALL);
	call->t.named.string = ident->string;
	call->t.named.list = args;
	return call;
}


static ast_node_t* parse_decl(bool single_assign, environment_t *env)
{
	token_t *ident = parse_ident(env);
	if (NULL == ident)
		return ast_error("expected ident", env);
	if (!check_and_skip(tok_EQ, env))
		return ast_error("expected '='", env);
	ast_node_t *expr = parse_expression(env);
	ast_node_t *decl = ast_node_create(ast_DECL);
	decl->t.named.single_assign = single_assign;
	decl->t.named.string = ident->string;
	decl->t.named.expr = expr;
	return decl;
}

static ast_node_t* parse_val_decl(environment_t *env)
{
	must_skip(tok_VAL, env);
	return parse_decl(true, env);
}

static ast_node_t* parse_var_decl(environment_t *env)
{
	must_skip(tok_VAR, env);
	return parse_decl(false, env);
}

static ast_node_t* parse_return(environment_t *env)
{
	must_skip(tok_RETURN, env);
	ast_node_t *expr = parse_expression(env);
	ast_node_t *ret = ast_node_create(ast_RETURN);
	ret->t.operands.op1 = expr;
	return ret;
}

static ast_node_t* parse_continue(environment_t *env)
{
	must_skip(tok_CONTINUE, env);
	return ast_node_create(ast_CONTINUE);
}

static ast_node_t* parse_break(environment_t *env)
{
	must_skip(tok_BREAK, env);
	return ast_node_create(ast_BREAK);
}

static ast_node_t* parse_block(environment_t *env)
{
	if (!check_and_skip(tok_CURLY_OPEN, env)) {
		return ast_error("expected block start", env);
	}
	if (check_and_skip(tok_CURLY_CLOSE, env)) {
		return ast_node_create(ast_NOP);
	}
	ast_node_vec_t stmts;
	kv_init(stmts);
	for (;;) {
		kv_push(ast_node_t*, stmts, parse_statement(env));
		if (check_and_skip(tok_CURLY_CLOSE, env)) break;
	}
	ast_node_t *block = ast_node_create(ast_BLOCK);
	block->t.named.list = stmts;
	return block;
}

static ast_node_t* parse_if(environment_t *env)
{
	must_skip(tok_IF, env);
	ast_node_t *cond = parse_expression(env);
	ast_node_t *then_block = parse_block(env);
	ast_node_t *else_block = NULL;
	if (check_and_skip(tok_ELSE, env)) {
		else_block = parse_block(env);
	}
	ast_node_t *ifte = ast_node_create(ast_IF);
	ifte->t.operands.op1 = cond;
	ifte->t.operands.op2 = then_block;
	ifte->t.operands.op3 = else_block;
	return ifte;
}

static ast_node_t* parse_for(environment_t *env)
{
	must_skip(tok_FOR, env);
	token_t *ident = parse_ident(env);
	if (NULL == ident)
		return ast_error("expected ident", env);
	if (!check_and_skip(tok_IN, env))
		return ast_error("expected 'in' keyword", env);
	ast_node_t *range = parse_expression(env);
	ast_node_t *body = parse_block(env);
	ast_node_t *finishblock = NULL;
	if (check_and_skip(tok_THEN, env)) {
		finishblock = parse_block(env);
	}
	ast_node_t *breakblock = NULL;
	if (check_and_skip(tok_ELSE, env)) {
		breakblock = parse_block(env);
	}
	ast_node_t *loop = ast_node_create(ast_FOR);
	loop->t.operands.string = ident->string;
	loop->t.operands.op1 = range;
	loop->t.operands.op2 = body;
	loop->t.operands.op3 = finishblock;
	loop->t.operands.op4 = breakblock;
	return loop;
}

static ast_node_t* parse_loop(environment_t *env)
{
	must_skip(tok_LOOP, env);
	ast_node_t *range = NULL;
	if (token(env)->type != tok_CURLY_OPEN) {
		range = parse_expression(env);
	}
	ast_node_t *body = parse_block(env);
	ast_node_t *finishblock = NULL;
	if (check_and_skip(tok_THEN, env)) {
		finishblock = parse_block(env);
	}
	ast_node_t *breakblock = NULL;
	if (check_and_skip(tok_ELSE, env)) {
		breakblock = parse_block(env);
	}
	ast_node_t *loop = ast_node_create(ast_LOOP);
	loop->t.operands.op1 = range;
	loop->t.operands.op2 = body;
	loop->t.operands.op3 = finishblock;
	loop->t.operands.op4 = breakblock;
	return loop;
}

static ast_node_t* parse_assignment(environment_t *env)
{
	token_t *ident = parse_ident(env);
	if (NULL == ident)
		return ast_error("expected ident", env);
	must_skip(tok_COLONEQ, env);
	ast_node_t *expr = parse_expression(env);
	ast_node_t *assign = ast_node_create(ast_ASSIGNMENT);
	assign->t.named.string = ident->string;
	assign->t.named.expr = expr;
	return assign;
}

static ast_node_t* parse_statement(environment_t *env)
{
	token_t *start = token(env);
	if (NULL == start) return ast_error("not a statement", env);
	switch (start->type) {
		case tok_VAL:        return parse_val_decl(env);
		case tok_VAR:        return parse_var_decl(env);
		case tok_RETURN:     return parse_return(env);
		case tok_CONTINUE:   return parse_continue(env);
		case tok_BREAK:      return parse_break(env);
		case tok_IF:         return parse_if(env);
		case tok_FOR:        return parse_for(env);
		case tok_LOOP:       return parse_loop(env);
		case tok_CURLY_OPEN: return parse_block(env);
		case tok_IDENT:;
			const token_t *next = peek(env, 1);
			switch (next->type) {
				case tok_PARENS_OPEN: return parse_function_call(env);
				case tok_COLONEQ: return parse_assignment(env);
				default: return ast_error("invalid statement starting with ident", env);
			}
		default:             return ast_error("invalid statement start", env);
	}
}

/**** INTERPRETER ****/

static unsigned execute_statement(environment_t *env, const ast_node_t *node)
{
	switch (node->type) {
	case ast_NONE:
		panic("inconsistent AST");
	case ast_ERROR:
		printf("Error: %s\n", node->t.named.string);
		return 1;
	case ast_NOP:
		return 0;
	case ast_DECL:
	case ast_BLOCK:
	case ast_IDENT:
	case ast_UNOP:
	case ast_BINOP:
	case ast_NUMBER:
	case ast_STRING:
	case ast_CALL:
	case ast_IF:
	case ast_FOR:
	case ast_LOOP:
	case ast_BREAK:
	case ast_CONTINUE:
	case ast_RETURN:
	case ast_TYPED:
	case ast_FUN:
	case ast_GEN:
	case ast_ASSIGNMENT:
	case ast_ARRAYACCESS:
	case ast_MULTI:
		panic("not implemented yet"); // FIXME
	}
	return 1;
}

/** Load and interpret a module */
unsigned load_file(const char *filename, environment_t *env)
{
	const int fd = open(filename, O_RDONLY);
	if (fd == -1) {
		return 1;
	}

	struct stat sb;
	if (fstat(fd, &sb) == -1) {
		return 2;
	}

	const size_t length = sb.st_size;
	const char *addr = mmap(NULL, length, PROT_READ, MAP_PRIVATE, fd, 0);
	if (addr == MAP_FAILED) {
		return 3;
	}

	env->lexer_pos = env->lexer_start = addr;
	lex(env);

	// TODO for debugging print all lexed tokens
	/*
	token_t *current = env->head_token;
	for (;;) {
		printf("[%d:%d] %d %s\n", current->line, current->column, current->type, current->string);
		if (current->next == NULL) break;
		current = current->next;
	}
	*/

	for (;;) {
		if (check_and_skip(tok_EOF, env)) break;
		ast_node_t *node = parse_statement(env);
		if (execute_statement(env, node) != 0) {
			return 6;
		}
	}

	return 0;
}

int main(int argc, const char *argv[]) {
	if (argc != 2) {
		printf("Usage: %s <filename>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	environment_t mainenv;
	environment_init(&mainenv);

	unsigned result = load_file(argv[1], &mainenv);
	if (result != 0) {
		printf("Failure while loading module '%s': %d\n", argv[1], result);
	}

	exit(EXIT_SUCCESS);
}
