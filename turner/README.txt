The Turner Language
===================

Wants to be a Lua competitor. So objectives are:

* Easy to learn (Java and JSON syntax)
* Simple to embed into C programs
* Good interoperability with C (caller and callee!)

What is unique about Turner?

* for i in range { S } then A else B
  If the loop successfully iterates through all of 'range', then A is executed.
  If the loop is prematurely exited via 'break', then B is executed.
  Likewise for 'loop n { S } then A else B'.
* Barely Turing-Complete
  Programs without 'loop { S }' always terminate.
  + easy analyses for execution time, memory need, etc.
  + sandboxing with closer integration
  - no recursion
  - no circular dependencies of modules, declarations, etc.
  - harder to program in some cases
* Interpret then Compile (?)
  A module is parsed and interpreted statement by statement.
  Afterwards the module is compiled to a more efficient representation.
  + (barely) Turing-Complete language at compile-time
    * Compute types?
    * Compare to (Lisp) macros?

