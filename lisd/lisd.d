abstract class BaseObject {
	BaseObject eval(BaseObject[string] env);
	BaseObject apply(BaseObject arg, BaseObject[string] env);
}

final class Atom : BaseObject {
	string val;
	this(string val) {
		this.val = val;
	}
	BaseObject eval(BaseObject[string] env) { return this; }
	BaseObject apply(BaseObject arg, BaseObject[string] env) {
		auto func = env[this.val];
		return func.apply(arg, env);
	}
}

unittest {
	auto a = new Atom("foo");
	auto b = new Atom("bar");
}

final class Cons : BaseObject {
	BaseObject car;
	BaseObject cdr;
	this(BaseObject a, BaseObject d) {
		this.car = a;
		this.cdr = d;
	}
	BaseObject eval(BaseObject[string] env) {
		return this.car.apply(this.cdr, env);
	}
	BaseObject apply(BaseObject arg, BaseObject[string] env) {
		auto func = this.car.eval(env);
		return func.apply(arg, env);
	}
}

final class Builtin : BaseObject {
	BaseObject function(BaseObject arg, BaseObject[string] env) applyf;
	this(BaseObject function(BaseObject arg, BaseObject[string] env) applyf) {
		this.applyf = applyf;
	}
	BaseObject eval(BaseObject[string] env) { return this; }
	BaseObject apply(BaseObject arg, BaseObject[string] env) {
		return this.applyf(arg, env);
	}
}

unittest {
	auto a = new Atom("foo");
	auto b = new Atom("bar");
	auto c1 = new Cons(a, b);
}

BaseObject[string] initEnv() {
	BaseObject[string] env;
	env["car"] = new Builtin(function(BaseObject arg, BaseObject[string] env) {
		auto cns = cast(Cons) arg;
		return cns.car;
	});
	env["cdr"] = new Builtin(function(BaseObject arg, BaseObject[string] env) {
		auto cns = cast(Cons) arg;
		return cns.cdr;
	});
	return env;
}

void main() {
}
