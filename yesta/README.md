Yesta Programming Language
--------------------------

A better C, but easy to smuggle into a C project.

* Excellent C interop
  * struct like C
  * zero-terminated strings
  * Compile to C?
  * C header generation from Yesta module
* Better than C
  * Const by default
  * Not-nullable pointer by default
  * Steal from D, meta programming
  * LL(1) parseable for good tools
  * Unicode?!
  * Generic Programming
  * Typeclasses

# Runtime Type Info

  struct Foo { int x; char y; }

Just a compile-time type in C.
Generates a runtime entity, so you can do

  x = Foo.size;
  for field in Foo.members

