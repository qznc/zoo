Notes from listening to Bjarne Stroustrup
"The Essence of C++: With Examples in C++84, C++98, C++11, and C++14"
http://channel9.msdn.com/Events/GoingNative/2013/Opening-Keynote-Bjarne-Stroustrup

# The argument for type classes

Bjarne: Template error messages are hidious. We need something more restrictive than "compile-time duck typing".
Bjarnes idea: Concepts(-lite) aka static if in D

Idea: Generics over type classes

fun sort(r:Range[Comparable]) { timsort... }
fun sort(r:Range[NaturalNumber]) { bucketsort... }

Example problem:
std::find (return first occurence) for sequence and for multiset
std::merge
std::accumulate

Challenge: Modular compilation vs instantiation

Idea: only parameterize modules?
- what about the different sort functions? Require them in a single module? :(
=> requires predicate style for external extension

# Rant about "paradigms"
