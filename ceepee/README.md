Ceepee Programming Language
===========================

Make continuation passing style bareable

Hello World
-----------

print ("Hello World");

Factorial
---------

proc range (a, b) {
	if (a=b) return[1] (); // use secondary continuation
	return[0] (a); // use primary continuation
	range (a+1, b);
}

proc factorial(n:int) {
	var result:int = 1;
	range(2..(n+1)) => (i) {
		result <- result * i;
	};
	return result;
}

Cat File
--------

Intuitive to write

	proc cat(path:string) {
		fh = open(path, 'r');
		data = read(fh);
		close(fh);
		print(data);
	}

Equivalent notation?

	proc cat(path:string) {
		open(path, 'r') => (fh) {
			read(fh) => (data) {
				close(fh);
				print (data);
			}
		}
	}

NQueens
-------

proc permute(positions:List[int]) {
	
}

proc solve(n:boardsize) {
	var count = 0;
	val positions = List(0..n);
	vec = permute(positions);
	i = range(0,n-1);
	j = range(i+1,n);
	if (vec[i]+1 != vec[j]+j and vec[i]-1 != vec[j]-j);
	count += 1;
}
